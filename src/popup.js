import React, {useState} from "react";
function Popup(){
    const [showPopup, setShowPopup] = useState(false);
    const togglePopup = () => {
        setShowPopup(!showPopup)
    }
    return (
        <>
        <div className="center-align">
        <button onClick={togglePopup}>Show Popup</button>
        </div>
                    
            {showPopup && (
              <>
                <div className="popup">
                    <div className="popup-inner">
                  <h2>Hello,Welcome!!!</h2>
                  <p>Click below to close</p>
                  <button onClick={togglePopup}>Close popup</button>
                  </div>
                </div>
                </>
            )}
       </>
    )
}
export default Popup